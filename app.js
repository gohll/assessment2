//Load express
var express = require("express");
//Create an instance of express application
var app = express();

var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var q = require('q');

var mysql = require("mysql");
var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "root",
    password: "669614",
    database: "booklibrary",
    connectionLimit: 4
});

const findAllLikeStmt = "SELECT id, author_lastname, author_firstname,title " +
    " FROM books WHERE title LIKE ? or author_lastname LIKE ? or author_firstname LIKE ? ORDER BY title ASC";
    // " FROM books WHERE title LIKE ? AND author_lastname LIKE ? AND author_firstname LIKE ? ORDER BY title ASC";
// const findOneStmt = "select id, title, author_lastname, author_firstname, cover_thumbnail from books where id = ?";
const findOneStmt = "select id, title, author_lastname, author_firstname from books where id = ?";

const postOneStmt = "update books set author_lastname = ?, author_firstname = ?, title = ? where id = ?";

var makeLikeQuery = function (sql, pool) {
    return (function (args) {
        var defer = q.defer();
        pool.getConnection(function (err, conn) {
            if (err) {
                return defer.reject(err);
            }
            var title_like = "%" + args.title + "%";
            var author_like = "%" + args.author + "%";
                
            conn.query( findAllLikeStmt, [title_like,  author_like, author_like], function (err, results) {
                conn.release();
                if (err) {
                    return defer.reject(err);
                }
                defer.resolve(results);
            });
        });
        return (defer.promise);
    });
};

var makeQuery = function (sql, pool) {
    return function (args) {
        var defer = q.defer();
        console.log(defer)
        pool.getConnection(function (err, conn) {
            if (err) {
                return defer.reject(err);
            }
            conn.query(sql, args || [], function (err, result) {
                conn.release();
                if (err) {
                    return defer.reject(err);
                }
                defer.resolve(result);
            });
        });
        return defer.promise;
    };
};
// read all based on like %?% criteria
var findAll = makeLikeQuery(findAllLikeStmt, pool);
var findOne = makeQuery(findOneStmt, pool);
var postOne = makeQuery(postOneStmt, pool);

app.get("/api/books", function (req, res) {
    // var limit = parseInt(req.query.limit) || 10;
    // var offset = parseInt(req.query.offset) || 0;
    var title = req.query.title_like
    var author = req.query.author_like;
    console.log(title, author)
    findAll({
        title: title,
        author: author
    })
        .then(function (books) {
            res.status(200).json(books);
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).end();
        });
});

app.get("/api/books/:bookid", function (req, res) {
    findOne([req.params.bookid])
        .then(function (book) {
            res.status(200).json(book[0]);
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).end();

        });
});


app.post("/api/book/update", function (req, res) {
    postOne([req.body.author_lastname, req.body.author_firstname, req.body.title, req.body.id])
        .then(function (book) {
            // res.status(200).json(book[]);
            res.status(200).json(book);
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).end();

        });
});
const PORT = "port";
app.use(express.static(__dirname + "/public"));

app.set(PORT, process.argv[2] || process.env.APP_PORT || 3000);

app.listen(app.get(PORT), function () {
    console.info("App Server started on " + app.get(PORT));
});

