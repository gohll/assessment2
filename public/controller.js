(function () {
    angular.module("BookApp")
        .controller("ListCtrl", ListCtrl)
        .controller("DetailCtrl", DetailCtrl);

    function ListCtrl(dbService, $state) {
        var vm = this;
        vm.search = {}

        vm.list = function() {
            dbService.list(vm.search)
                .then(function (books) {
                    vm.books = books;
                })
                .catch(function (err) {
                    console.log("Error occurred. Listing of books not sucessful.", err);
                });
        }

        vm.getDetails = function (Id) {
            console.log(Id)
            $state.go("detail", {bookId: Id});
        }

    }

    ListCtrl.$inject = ["dbService", "$state"];

    function DetailCtrl($stateParams, dbService, $http) {
        var vm = this;
        vm.result = {};
        vm.getBook = function(){
            return dbService.book
        }


        dbService.detail($stateParams.bookId)
            .then(function (book) {
                vm.book = book;
            });
        
        vm.update = function(){
            dbService.update(vm.book)
        }


        // vm.cancel = function () {
        //         return window.location = "/views/list.html"
        // }; 
   
    }

    DetailCtrl.$inject = ["$stateParams", "dbService"];
}());