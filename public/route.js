(function () {

    angular
        .module("BookApp")
        .config(BookConfig);

    function BookConfig($stateProvider, $urlRouterProvider) {
        console.log("asdfasd")
        $stateProvider
            .state("list", {
                url: "/",
                templateUrl: "/views/list.html",
                controller: "ListCtrl as ctrl"
            })
            .state("detail", {
                url: "/detail/:bookId",
                templateUrl: "/views/details.html",
                controller: "DetailCtrl as ctrl"
            });

        $urlRouterProvider.otherwise("/");
    }

    BookConfig.$inject = ["$stateProvider", "$urlRouterProvider"];


})();