(function () {
    angular.module("BookApp")
        .service("dbService",["$http","$state", "$q", dbService]);

    function dbService($http, $state, $q) {
        var vm = this;

        vm.books = [];

        vm.getBook = function (){
            var book = {};
            book.id = "";
            book.title = "";
            book.author_lastname = "";
            book.author_firstname = "";
            return book;
        };
        
        vm.list = function (search) {
            var defer = $q.defer();
            var params = {
                title_like: search.title_like,
                author_like: search.author_like,
                limit: 10,
                offset: 0
            };

            $http.get("/api/books", {
                params: params
            }).then(function (results) {
                vm.books = results.data
                console.log("data", results.data)
                defer.resolve(results.data);
            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;
        };

        vm.detail = function (Id) {
            var defer = $q.defer();

            $http.get("/api/books/" + Id)
                .then(function (result) {
                    vm.book = result.data
                    defer.resolve(result.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };

        vm.update = function (book) {
            var defer = $q.defer();

            $http.post("/api/book/update", book)
                .then(function (result) {
                    defer.resolve(result.data);
                    $state.go("list")
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };

    }

    dbService.$inject = ["$http", "$q"];
})();